﻿using Student.Library.Utilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Student.Web.Controllers
{
    public class EnrollmentController : Controller
    {

        //Properties        
        static string baseUrl = ConfigurationManager.AppSettings["WebApiUrl"].ToString();

        EnrollmentClient _enrollmentClient = new EnrollmentClient(baseUrl);
        
        //
        // GET: /Enrollment/
        [Authorize(Roles="B")]
        public ActionResult Index()
        {
            var list = _enrollmentClient.GetAll();
            return View(list);
        }

    }
}

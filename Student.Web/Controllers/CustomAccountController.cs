﻿using Student.Library;
using Student.Library.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WebMatrix.WebData;

namespace Student.Web.Controllers
{
    [AllowAnonymous]
    public class CustomAccountController : Controller
    {
        private UsersDAL _usersDAL;

        public CustomAccountController()
        {
            this._usersDAL = new UsersDAL();
        }

        //
        // GET: /CustomAccount/

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(User user)
        {
            user=_usersDAL.GetUser(user.UserName, user.PassWord);
            if(user==null)
            {
                ViewBag.errorMsg = "Invalid User";
                return View();
            }
            else
            {
                FormsAuthentication.SetAuthCookie(user.UserName, false);
                return RedirectToAction("Index", "Students");
            } 
        }

        // POST: /Account/LogOff

        [HttpPost]        
        public ActionResult LogOut()
        {
            WebSecurity.Logout();
            return RedirectToAction("Index", "Students");
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Student.Library;
using Student.Library.DAL;
using System.Configuration;
using RestSharp.Authenticators;
using Student.Library.Utilities;


namespace Student.Web.Controllers
{
    [AllowAnonymous]
    public class StudentsController : Controller
    {
        //Properties        
        static string baseUrl = ConfigurationManager.AppSettings["WebApiUrl"].ToString();

        StudentClient _studentClient = new StudentClient(baseUrl);       

        //
        // GET: /Students/
        public ActionResult Index()
        {
            var list = _studentClient.GetAll();
            return View(list);
        }

         public ActionResult Create()
        {
            return View();
        }


        [HttpPost]
        public ActionResult Create(FormCollection fields,Students students)
        {
            fields["FirstName"] = students.FirstName;
            fields["LastName"] = students.LastName;
            fields["EnrollmentDate"] = Convert.ToString(students.EnrollmentDate);   
            _studentClient.CreateStudents(students);
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            var student = _studentClient.GetStudent(id);
            if(student==null)
            {
                return HttpNotFound();
            }
            return View(student);
        }

        [HttpPost]
        public ActionResult Edit(FormCollection fields, Students students)
        {
            fields["FirstName"] = students.FirstName;
            fields["LastName"] = students.LastName;
            fields["EnrollmentDate"] = Convert.ToString(students.EnrollmentDate);
            _studentClient.PutStudents(students);
            return RedirectToAction("Index");
        }
    }
}

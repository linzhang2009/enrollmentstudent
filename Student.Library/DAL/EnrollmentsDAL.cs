﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Student.Library.DAL
{
    public class EnrollmentsDAL
    {
        public List<Enrollments> GetAllEnrollments()
        {
            using (var db = new OrganizationEntities())
            {
                //AsNoTracking()can save both execution times and memory usage. when we retrieve a large amount of data from the database.
                var list = db.Enrollments.Include("Students").Include("Courses").ToList();
              //  var list = db.Enrollments.ToList();
                //Store Procedure
               // var list = db.SP_GetAllEnrollements().ToList();

                return list;
            }
        }

        public Enrollments Get(int id)
        {
            using (var db = new OrganizationEntities())
            {
                //var enrollment = db.Enrollments.Include("Students").Include("Courses").Where(x => x.EnrollmentID == id).FirstOrDefault();
                var enrollment = db.Enrollments.Where(x => x.EnrollmentID == id).FirstOrDefault();
                return enrollment;
            }
        }


        public Enrollments GetByStudent(int studentId)
        {
            using (var db = new OrganizationEntities())
            {
                var res = db.Enrollments.Where(x => x.StudentID == studentId).FirstOrDefault();
                return res;
            }
        }

        public List<Enrollments> SortGrade(List<Enrollments> enrollments, string sort)
        {
            switch(sort)
            {
                case "gradeAsc":
                    return enrollments.OrderBy(g => g.Grade).ToList();
                case"gradeDec" :
                    return enrollments.OrderByDescending(g => g.Grade).ToList();
                default:
                    return enrollments.OrderBy(g => g.EnrollmentID).ToList();
            }
        }



            
        public Enrollments Add(Enrollments enrollments)
        {
            using (var db = new OrganizationEntities())
            {
                db.Enrollments.Add(enrollments);
                db.SaveChanges();
                return enrollments;
            }
        }

        public Enrollments Update(Enrollments enrollments)
        {
            using (var db = new OrganizationEntities())
            {
                var res=db.Enrollments.Where(x => x.EnrollmentID == enrollments.EnrollmentID).FirstOrDefault();
                if(res!=null)
                {
                    res.CourseID = enrollments.CourseID;
                    res.StudentID = enrollments.StudentID;
                    res.Grade = enrollments.Grade;
                    db.SaveChanges();
                }
                else
                {
                    throw new InvalidDataException("Enrollments Does Not Exsit");
                }
                return enrollments;
            }
        }

        public void Delete(int enrollmentId)
        {
            using (var db = new OrganizationEntities())
            {
                var res = db.Enrollments.Where(x => x.EnrollmentID == enrollmentId).FirstOrDefault();
                if(res!=null)
                {
                    db.Enrollments.Remove(res);
                    db.SaveChanges();
                }
            }
        }

    }
}

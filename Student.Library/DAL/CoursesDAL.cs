﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Student.Library.DAL
{
    public class CoursesDAL
    {
        public List<Courses> GetAllEnrollments()
        {
            using (var db = new OrganizationEntities())
            {
                //AsNoTracking()can save both execution times and memory usage. when we retrieve a large amount of data from the database.
                var list = db.Courses.ToList();


                return list;
            }
        }



        public Courses Get(int id)
        {
            using (var db = new OrganizationEntities())
            {
                var courses = db.Courses.Where(x => x.CourseID== id).FirstOrDefault();
                return courses;
            }
        }


        public Courses Add(Courses courses)
        {
            using (var db = new OrganizationEntities())
            {
                db.Courses.Add(courses);
                db.SaveChanges();
                return courses;
            }
        }

        public Courses Update(Courses courses)
        {
            using (var db = new OrganizationEntities())
            {
                var res = db.Courses.Where(x => x.CourseID == courses.CourseID).FirstOrDefault();
                if (res != null)
                {
                    res.Title = courses.Title;
                    res.Credits = courses.Credits;
                    db.SaveChanges();
                }
                else
                {
                    throw new InvalidDataException("Courses Does Not Exsit");
                }
                return courses;
            }
        }

        public void Delete(int coursesId)
        {
            using (var db = new OrganizationEntities())
            {
                var res = db.Courses.Where(x => x.CourseID == coursesId).FirstOrDefault();
                if (res != null)
                {
                    db.Courses.Remove(res);
                    db.SaveChanges();
                }
            }
        }
    }
}

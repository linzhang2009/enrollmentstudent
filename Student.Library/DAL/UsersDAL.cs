﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Student.Library.DAL
{
    public class UsersDAL
    {
        public List<User> GetAllUsers()
        {
            using (var db = new OrganizationEntities())
            {
                //AsNoTracking()can save both execution times and memory usage. when we retrieve a large amount of data from the database.
                var list = db.Users.ToList();
                return list;
            }
        }

        public String GetUserRole(string userName)
        {
            using (var db = new OrganizationEntities())
            {
                string Role = db.Users.Where(x => x.UserName == userName).FirstOrDefault().Role;
                return Role;
            }
        }


        public User GetUser(string userName,string passWord)
        {
            using (var db = new OrganizationEntities())
            {
                var user = db.Users.Where(x => x.UserName == userName && x.PassWord == passWord).FirstOrDefault();               
                return user;
            }
        }
    }
}

﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;

namespace Student.Library.Utilities
{
    public class StudentClient
    {

          //properties
        private  RestClient _client;

        //constructors
        public StudentClient(string baseUrl)
        {
            _client = new RestClient(baseUrl);           
        }


        //Get//api/student
        public List<Students> GetAll()
        {
            RestRequest request = new RestRequest(Method.GET)
            {
                RequestFormat = DataFormat.Json,
                Resource = "api/students"
            };

            IRestResponse<List<Students>> response = _client.Execute<List<Students>>(request);
            List<Students> studentsList = response.Data;
            return studentsList;
        }


        //api/student/id
        public Students GetStudent(int studentsId)
        {
            RestRequest request = new RestRequest(Method.GET)
            {
                RequestFormat = DataFormat.Json,
                Resource = string.Format("api/students/{0}", studentsId)
            };
            IRestResponse<Students> response = _client.Execute<Students>(request);
            return response.Data;
        }

        public Students GetStudentFirst(string FirstName)
        {
            RestRequest request = new RestRequest(Method.GET)
            {
                RequestFormat = DataFormat.Json,
                Resource = string.Format("api/students/{0}", FirstName)
            };
            IRestResponse<Students> response = _client.Execute<Students>(request);
            return response.Data;
        }

        /*
        //api/studentId?id=ID
        public List<Students> GetEnrollmentsbyStudentId(int id)
        {
            RestRequest request = new RestRequest(Method.GET)
            {
                RequestFormat = DataFormat.Json,
                Resource = string.Format("api/enrollments?id={0}", id)
            };
            IRestResponse<List<Enrollments>> response = _client.Execute<List<Enrollments>>(request);
            List<Enrollments> enrollmentList = response.Data;
            return enrollmentList;
        }*/


        //POST for Create  // api/students 
        public Students CreateStudents(Students students)
        {
            RestRequest request = new RestRequest(Method.POST)
            {
                Resource = "api/students",
                RequestFormat = DataFormat.Json
            };
            request.AddBody(students);

            IRestResponse<Students> response = _client.Execute<Students>(request);

            return response.Data;
        }


        //PUT FOR UPDATE // api/products/id 
        //returned from the API
        public Students PutStudents(Students students)
        {
            RestRequest request = new RestRequest(Method.PUT)
            {
                Resource = "api/students/" + students.ID,
                RequestFormat = DataFormat.Json
            };
            request.AddBody(students);

            IRestResponse<Students> response = _client.Execute<Students>(request);

            return response.Data;
        }



        //Delete        
        public HttpStatusCode DeleteStudent(int Id)
        {
            var request = new RestRequest(Method.DELETE)
            {
                Resource = "api/students/" + Id
            };
            var response = _client.Execute<Students>(request);
            return response.StatusCode;
        }
    }
}

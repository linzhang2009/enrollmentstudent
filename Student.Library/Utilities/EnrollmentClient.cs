﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;

namespace Student.Library.Utilities
{
    public class EnrollmentClient
    {

         //properties
        private readonly RestClient _client;

        //constructors
        public EnrollmentClient(string baseUrl)
        {
            _client = new RestClient(baseUrl);           
        }


        //Get//api/products
        public List<Enrollments> GetAll()
        {
            RestRequest request = new RestRequest(Method.GET)
            {
                RequestFormat = DataFormat.Json,
                Resource = "api/enrollments"
            };

            IRestResponse<List<Enrollments>> response = _client.Execute<List<Enrollments>>(request);
            List<Enrollments> enrollmentsList = response.Data;
            return enrollmentsList;
        }


        //api/product/id
        public Enrollments GetEnrollment(int enrollmentsId)
        {
            RestRequest request = new RestRequest(Method.GET)
            {
                RequestFormat = DataFormat.Json,
                Resource = string.Format("api/enrollments/{0}", enrollmentsId)
            };
            IRestResponse<Enrollments> response = _client.Execute<Enrollments>(request);
            return response.Data;
        }


        //api/StudentId?id=ID
        public List<Enrollments> GetEnrollmentsbyStudentId(int id)
        {
            RestRequest request = new RestRequest(Method.GET)
            {
                RequestFormat = DataFormat.Json,
                Resource = string.Format("api/enrollments?id={0}", id)
            };
            IRestResponse<List<Enrollments>> response = _client.Execute<List<Enrollments>>(request);
            List<Enrollments> enrollmentList = response.Data;
            return enrollmentList;
        }


        //POST for Create  // api/products 
        public Enrollments CreateEnrollments(Enrollments enrollments)
        {
            RestRequest request = new RestRequest(Method.POST)
            {
                Resource = "api/enrollments",
                RequestFormat = DataFormat.Json
            };
            request.AddBody(enrollments);

            IRestResponse<Enrollments> response = _client.Execute<Enrollments>(request);

            return response.Data;
        }


        //PUT FOR UPDATE // api/products/id 
        //returned from the API
        public Enrollments PutEnrollments(Enrollments enrollments)
        {
            RestRequest request = new RestRequest(Method.PUT)
            {
                Resource = "api/enrollments/" + enrollments.EnrollmentID,
                RequestFormat = DataFormat.Json
            };
            request.AddBody(enrollments);

            IRestResponse<Enrollments> response = _client.Execute<Enrollments>(request);

            return response.Data;
        }



        //Delete        
        public HttpStatusCode DeleteProduct(int enrollmentsId)
        {
            var request = new RestRequest(Method.DELETE)
            {
                Resource = "api/enrollments/" + enrollmentsId
            };
            var response = _client.Execute<Enrollments>(request);
            return response.StatusCode;
        }
    }
}

﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;

namespace Student.Library.Utilities
{
    public class CourseClient
    {
          //properties
        private readonly RestClient _client;

        //constructors
        public CourseClient(string baseUrl)
        {
            _client = new RestClient(baseUrl);           
        }


        //Get//api/products
        public List<Courses> GetAll()
        {
            RestRequest request = new RestRequest(Method.GET)
            {
                RequestFormat = DataFormat.Json,
                Resource = "api/courses"
            };

            IRestResponse<List<Courses>> response = _client.Execute<List<Courses>>(request);
            List<Courses> coursesList = response.Data;
            return coursesList;
        }


        //api/product/id
        public Courses GetCourses(int coursesId)
        {
            RestRequest request = new RestRequest(Method.GET)
            {
                RequestFormat = DataFormat.Json,
                Resource = string.Format("api/courses/{0}", coursesId)
            };
            IRestResponse<Courses> response = _client.Execute<Courses>(request);
            return response.Data;
        }


        //api/StudentId?id=ID
        public List<Courses> GetCoursesbyId(int id)
        {
            RestRequest request = new RestRequest(Method.GET)
            {
                RequestFormat = DataFormat.Json,
                Resource = string.Format("api/courses?id={0}", id)
            };
            IRestResponse<List<Courses>> response = _client.Execute<List<Courses>>(request);
            List<Courses> coursesList = response.Data;
            return coursesList;
        }


        //POST for Create  // api/products 
        public Courses CreateCourses(Courses courses)
        {
            RestRequest request = new RestRequest(Method.POST)
            {
                Resource = "api/courses",
                RequestFormat = DataFormat.Json
            };
            request.AddBody(courses);

            IRestResponse<Courses> response = _client.Execute<Courses>(request);

            return response.Data;
        }


        //PUT FOR UPDATE // api/products/id 
        //returned from the API
        public Courses PutCourses(Courses courses)
        {
            RestRequest request = new RestRequest(Method.PUT)
            {
                Resource = "api/courses/" + courses.CourseID,
                RequestFormat = DataFormat.Json
            };
            request.AddBody(courses);

            IRestResponse<Courses> response = _client.Execute<Courses>(request);

            return response.Data;
        }



        //Delete        
        public HttpStatusCode DeleteCourses(int coursesId)
        {
            var request = new RestRequest(Method.DELETE)
            {
                Resource = "api/enrollments/" + coursesId
            };
            var response = _client.Execute<Courses>(request);
            return response.StatusCode;
        }
    }
}

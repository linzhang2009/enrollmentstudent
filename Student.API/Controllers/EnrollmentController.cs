﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Student.Library;
using Student.Library.DAL;

namespace Student.API.Controllers
{
    public class EnrollmentController : ApiController
    {
        //Properties
        private EnrollmentsDAL enrollmentsDAL;

        //Constructor
        public EnrollmentController()
        {
            this.enrollmentsDAL = new EnrollmentsDAL();
        }

        //api/enrollment
        public List<Enrollments> GetAllEnrollment()
        {
            var enrollments = enrollmentsDAL.GetAllEnrollments();
            return enrollments;
        }

        public Enrollments GetById(int EnrollmentID)
        {
            var res = enrollmentsDAL.Get(EnrollmentID);
            if(res==null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
            return res;
        }



    }
}

﻿using Student.Library;
using Student.Library.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Student.API.Controllers
{

    public class StudentsController : ApiController
    {
        //property
        private StudentsDAL studentsDAL;
        //constructor
        public StudentsController()
        {
            this.studentsDAL = new StudentsDAL();
        }


        public List<Students> GetAll()
        {
            var studentsList = studentsDAL.GetAllStudents();
            return studentsList;
        }


        public Students GetById(int ID)
        {
            var student = studentsDAL.Get(ID);
            if (student == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
            return student;
        }


        public Students GetByFirst(string FirstName)
        {
            var student = studentsDAL.GetByFirst(FirstName);
            if (student == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
            return student;
        }


        //CRUD
        //Post for Create
        public HttpResponseMessage PostStudent(Students student)
        {
            studentsDAL.Add(student);
            if (student != null)
            {
                var response = Request.CreateResponse<Students>(HttpStatusCode.Created, student);
                string uri = Url.Link("DefaultApi", new { ID = student.ID });
                response.Headers.Location = new Uri(uri);
                return response;
            }
            else
                throw new HttpResponseException(HttpStatusCode.InternalServerError);
        }


        //PUT FOR UPDATE
        public void PutStudents(Students student)
        {
            studentsDAL.Update(student);
            if(student==null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }

    }

}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace Student.API
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            var json = config.Formatters.JsonFormatter;
            json.SerializerSettings.PreserveReferencesHandling = Newtonsoft.Json.PreserveReferencesHandling.Objects;
            config.Formatters.Remove(config.Formatters.XmlFormatter);

            /*
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
               // routeTemplate: "api/{controller}/{id}",
              //  defaults: new { id = RouteParameter.Optional }
              routeTemplate: "api/{controller}/{id}",
              defaults: new { ID = RouteParameter.Optional }
            );*/

            config.Routes.MapHttpRoute(
               name: "Students",
               routeTemplate: "api/students/{ID}",
               defaults: new { controller = "students", ID = RouteParameter.Optional }
           );


            
            config.Routes.MapHttpRoute(
                name: "Student",
                routeTemplate:"api/student/{FirstName}",
                defaults: new { controller = "students", FirstName = RouteParameter.Optional}
            );

            config.Routes.MapHttpRoute(
              name: "Enrollment",
              routeTemplate: "api/enrollment/{EnrollmentID}",
              defaults: new { controller = "enrollment", EnrollmentID = RouteParameter.Optional }
          );

            // Uncomment the following line of code to enable query support for actions with an IQueryable or IQueryable<T> return type.
            // To avoid processing unexpected or malicious queries, use the validation settings on QueryableAttribute to validate incoming queries.
            // For more information, visit http://go.microsoft.com/fwlink/?LinkId=279712.
            //config.EnableQuerySupport();

            // To disable tracing in your application, please comment out or remove the following line of code
            // For more information, refer to: http://www.asp.net/web-api
            config.EnableSystemDiagnosticsTracing();

           
        }
    }
}
